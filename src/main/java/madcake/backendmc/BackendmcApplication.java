package madcake.backendmc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendmcApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendmcApplication.class, args);
    }

}
