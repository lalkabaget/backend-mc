package madcake.backendmc.controller;

import madcake.backendmc.domain.models.request.AuthRequest;
import madcake.backendmc.domain.models.response.AuthResponse;
import madcake.backendmc.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/auth")
    public ResponseEntity<AuthResponse> login(@RequestBody AuthRequest authRequest) {
       return ResponseEntity.ok(authService.getToken(authRequest));
    }
}
