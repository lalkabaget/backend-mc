package madcake.backendmc.controller;

import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.models.SessionCart;
import madcake.backendmc.domain.models.response.CartItemResponse;
import madcake.backendmc.exceptions.SessionCartException;
import madcake.backendmc.service.SessionCartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class CartController {
    private final SessionCartItemService sessionCartItemService;

    @Autowired
    public CartController(SessionCartItemService sessionCartItemService) {
        this.sessionCartItemService = sessionCartItemService;
    }

    @PostMapping("/cart-item/add")
    public ResponseEntity<CartItemResponse> addCartItem(HttpSession session, @RequestBody ProductDTO product) {
        SessionCart sessionCart = sessionCartItemService.addSessionCartItem(session, product.getId());
        CartItemResponse cartItemResponse = new CartItemResponse(sessionCart);
        return ResponseEntity.ok(cartItemResponse);
    }

    @DeleteMapping("/cart-item/{id}")
    public ResponseEntity<CartItemResponse> removeCartItem(HttpSession session,
                                                           @PathVariable("id") Long productId) throws SessionCartException {
        SessionCart sessionCart = sessionCartItemService.removeSessionCartItem(session, productId);
        CartItemResponse cartItemResponse = new CartItemResponse(sessionCart);
        return ResponseEntity.ok(cartItemResponse);
    }
}
