package madcake.backendmc.controller;

import madcake.backendmc.domain.dto.ClientDTO;
import madcake.backendmc.domain.entity.ClientEntity;
import madcake.backendmc.domain.models.response.RegistrationResponse;
import madcake.backendmc.exceptions.ClientServiceException;
import madcake.backendmc.service.ClientService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class ClientController {
    private final ClientService clientService;
    private final ConversionService conversionService;
    private final RabbitTemplate rabbitTemplate;


    @Autowired
    public ClientController(ClientService clientService,
                            ConversionService conversionService,
                            RabbitTemplate rabbitTemplate) {
        this.clientService = clientService;
        this.conversionService = conversionService;
        this.rabbitTemplate = rabbitTemplate;

    }

    @PostMapping("/registration")
    public ResponseEntity<RegistrationResponse> regist(@RequestBody ClientDTO client) throws ClientServiceException {
        ClientEntity registeredClient = clientService.register(client);
        //rabbitTemplate.convertAndSend("mailQueue",registeredClient.getUsername());
        return ResponseEntity.ok( new RegistrationResponse(conversionService.convert(registeredClient, ClientDTO.class)));
    }



}
