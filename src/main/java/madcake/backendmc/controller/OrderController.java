package madcake.backendmc.controller;

import madcake.backendmc.domain.dto.AddressDTO;
import madcake.backendmc.domain.dto.OrderDTO;
import madcake.backendmc.domain.entity.AddressEntity;
import madcake.backendmc.domain.entity.OrderEntity;
import madcake.backendmc.domain.models.response.NewOrderResponse;
import madcake.backendmc.service.OrderService;
import madcake.backendmc.service.SessionCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderController {
    private final OrderService orderService;
    private final SessionCartService sessionCartService;
    private final ConversionService conversionService;


    @Autowired
    public OrderController(OrderService orderService, SessionCartService sessionCartService, ConversionService conversionService) {
        this.orderService = orderService;
        this.sessionCartService = sessionCartService;
        this.conversionService = conversionService;
    }

    @PostMapping("/order")
    public ResponseEntity<NewOrderResponse> doOrder(HttpSession session, Principal principal, @RequestBody AddressDTO address) {
        OrderEntity order =
                orderService.addOrder(sessionCartService.getSessionCart(session),
                        conversionService.convert(address, AddressEntity.class),
                        principal.getName());

        return ResponseEntity.ok(new NewOrderResponse(conversionService.convert(order, OrderDTO.class)));
    }



}
