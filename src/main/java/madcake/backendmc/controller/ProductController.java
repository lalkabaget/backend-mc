package madcake.backendmc.controller;

import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.entity.ProductEntity;
import madcake.backendmc.domain.models.SessionCart;
import madcake.backendmc.domain.models.response.ProductByIdResponse;
import madcake.backendmc.domain.models.response.ProductsResponse;
import madcake.backendmc.service.ProductService;
import madcake.backendmc.service.SessionCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {
    private final ProductService productService;
    private final ConversionService conversionService;
    private final SessionCartService sessionCartService;


    @Autowired
    public ProductController(ProductService productService, ConversionService conversionService, SessionCartService sessionCartService) {
        this.productService = productService;
        this.conversionService = conversionService;
        this.sessionCartService = sessionCartService;
    }

    @GetMapping("/products")
    public ResponseEntity<ProductsResponse> products(HttpSession session) {
        SessionCart sessionCart = sessionCartService.getSessionCart(session);

        List<ProductDTO> allProducts =
                productService.getAllProducts()
                        .stream()
                        .map(it -> conversionService.convert(it, ProductDTO.class))
                        .collect(Collectors.toList());

        return ResponseEntity.ok(new ProductsResponse(allProducts, sessionCart));
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<ProductByIdResponse> productById(HttpSession session, @PathVariable Long id) {
        SessionCart sessionCart = sessionCartService.getSessionCart(session);
        ProductEntity productById = productService.getProductById(id);
        return ResponseEntity.ok(new ProductByIdResponse(conversionService.convert(productById, ProductDTO.class), sessionCart));
    }

    @GetMapping("/products-category/{groupTitle}")
    public ResponseEntity<ProductsResponse> productsByGroup(HttpSession session, @PathVariable String groupTitle) {
        SessionCart sessionCart = sessionCartService.getSessionCart(session);

        List<ProductDTO> productsByGroup =
                productService.getProductsByGroup(groupTitle)
                        .stream()
                        .map(it -> conversionService.convert(it, ProductDTO.class))
                        .collect(Collectors.toList());

        return ResponseEntity.ok(new ProductsResponse(productsByGroup, sessionCart));
    }


}
