package madcake.backendmc.converter;

import madcake.backendmc.domain.entity.AddressEntity;
import madcake.backendmc.domain.dto.AddressDTO;
import madcake.backendmc.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressDtoToAddressEntityConverter extends SelfRegisterConverter<AddressDTO, AddressEntity> {
    @Override
    public AddressEntity convert(AddressDTO source) {
        AddressEntity target = new AddressEntity();

        target.setId(source.getId());
        target.setStreet(source.getStreet());
        target.setHouseNum(source.getHouseNum());
        target.setBuildingNum(source.getBuildingNum());
        target.setPorch(source.getPorch());
        target.setFloor(source.getFloor());
        target.setRoom(source.getRoom());

        return target;
    }
}
