package madcake.backendmc.converter;

import madcake.backendmc.domain.entity.AddressEntity;
import madcake.backendmc.domain.dto.AddressDTO;
import org.springframework.stereotype.Component;
@Component
public class AddressEntityToAddressDtoConverter extends SelfRegisterConverter<AddressEntity, AddressDTO> {
    @Override
    public AddressDTO convert(AddressEntity source) {
        AddressDTO target = new AddressDTO();
        target.setId(source.getId());
        target.setStreet(source.getStreet());
        target.setHouseNum(source.getHouseNum());
        target.setBuildingNum(source.getBuildingNum());
        target.setPorch(source.getPorch());
        target.setFloor(source.getFloor());
        target.setRoom(source.getRoom());

        return target;
    }
}
