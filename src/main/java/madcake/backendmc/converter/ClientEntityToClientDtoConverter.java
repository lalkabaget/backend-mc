package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.ClientDTO;
import madcake.backendmc.domain.entity.ClientEntity;
import org.springframework.stereotype.Component;

@Component
public class ClientEntityToClientDtoConverter extends SelfRegisterConverter<ClientEntity, ClientDTO> {
    @Override
    public ClientDTO convert(ClientEntity source) {
        ClientDTO target = new ClientDTO();
        target.setId(source.getId());
        target.setName(source.getName());
        target.setPhone(source.getPhone());
        target.setEmail(source.getEmail());
        target.setUsername(source.getUsername());
        target.setPassword(source.getPassword());

        return target;
    }
}
