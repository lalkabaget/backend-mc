package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.ClientDTO;
import madcake.backendmc.domain.dto.OrderItemDTO;
import madcake.backendmc.domain.entity.OrderEntity;
import madcake.backendmc.domain.entity.OrderItemEntity;
import madcake.backendmc.domain.dto.AddressDTO;
import madcake.backendmc.domain.dto.OrderDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderEntityToOrderDtoConverter extends SelfRegisterConverter<OrderEntity, OrderDTO> {
    @Override
    public OrderDTO convert(OrderEntity source) {
        OrderDTO target = new OrderDTO();
        target.setId(source.getId());
        target.setClient(conversionService.convert(source.getClient(), ClientDTO.class));
        target.setDateTime(source.getDateTime());
        target.setAddress(conversionService.convert(source.getAddress(), AddressDTO.class));
        target.setPaymentType(source.getPaymentType());
        target.setTotal(source.getTotal());

        List<OrderItemDTO> orderItems = new ArrayList<>();
        if(source.getOrderItems()!=null) {
            for (OrderItemEntity orderItem : source.getOrderItems())
                orderItems.add(conversionService.convert(orderItem, OrderItemDTO.class));
        }
        target.setOrderItems(orderItems);

        return target;
    }


}
