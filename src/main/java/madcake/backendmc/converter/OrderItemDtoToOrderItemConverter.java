package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.OrderItemDTO;
import madcake.backendmc.domain.entity.OrderItemEntity;
import madcake.backendmc.domain.entity.ProductEntity;
import org.springframework.stereotype.Component;

@Component
public class OrderItemDtoToOrderItemConverter extends SelfRegisterConverter<OrderItemDTO, OrderItemEntity> {

    @Override
    public OrderItemEntity convert(OrderItemDTO source) {
        OrderItemEntity target = new OrderItemEntity();
        target.setId(source.getId());
        target.setQuantity(source.getQuantity());
        target.setProduct(conversionService.convert(source.getProduct(), ProductEntity.class));
        return target;
    }
}
