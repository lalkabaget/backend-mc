package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.entity.OrderItemEntity;
import madcake.backendmc.domain.dto.OrderItemDTO;
import org.springframework.stereotype.Component;

@Component
public class OrderItemToOrderItemDtoConverter extends SelfRegisterConverter<OrderItemEntity, OrderItemDTO> {
    @Override
    public OrderItemDTO convert(OrderItemEntity source) {
        OrderItemDTO target = new OrderItemDTO();
        target.setId(source.getId());
        target.setQuantity(source.getQuantity());
        target.setProduct(conversionService.convert(source.getProduct(), ProductDTO.class));
        return target;
    }
}
