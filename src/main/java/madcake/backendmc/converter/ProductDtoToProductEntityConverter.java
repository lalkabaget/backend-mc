package madcake.backendmc.converter;

import madcake.backendmc.domain.entity.ProductEntity;
import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.entity.ProductGroupEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductDtoToProductEntityConverter extends SelfRegisterConverter<ProductDTO, ProductEntity> {
    @Override
    public ProductEntity convert(ProductDTO source) {
        ProductEntity target = new ProductEntity();
        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setDescription(source.getDescription());
        target.setPrice(source.getPrice());
        target.setProductGroup(conversionService.convert(source.getProductGroup(), ProductGroupEntity.class));
        return target;
    }
}
