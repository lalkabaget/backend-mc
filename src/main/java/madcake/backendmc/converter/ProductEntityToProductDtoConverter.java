package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.ProductGroupDTO;
import madcake.backendmc.domain.entity.ProductEntity;
import madcake.backendmc.domain.dto.ProductDTO;
import org.springframework.stereotype.Component;

@Component
public class ProductEntityToProductDtoConverter extends SelfRegisterConverter<ProductEntity, ProductDTO> {
    @Override
    public ProductDTO convert(ProductEntity source) {
        ProductDTO target = new ProductDTO();

        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setDescription(source.getDescription());
        target.setPrice(source.getPrice());
        target.setProductGroup(conversionService.convert(source.getProductGroup(), ProductGroupDTO.class));

        return target;
    }
}
