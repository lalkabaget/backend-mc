package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.ProductGroupDTO;
import madcake.backendmc.domain.entity.ProductGroupEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductGroupDtoToProductGroupEntityConverter extends  SelfRegisterConverter<ProductGroupDTO, ProductGroupEntity> {
    @Override
    public ProductGroupEntity convert(ProductGroupDTO source) {
        ProductGroupEntity target = new ProductGroupEntity();
        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setDescription(source.getDescription());

        return target;
    }
}
