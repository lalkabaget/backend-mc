package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.ProductGroupDTO;
import madcake.backendmc.domain.entity.ProductGroupEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductGroupEntityToProductGroupDtoConverter extends SelfRegisterConverter<ProductGroupEntity, ProductGroupDTO> {
    @Override
    public ProductGroupDTO convert(ProductGroupEntity source) {
        ProductGroupDTO target = new ProductGroupDTO();
        target.setId(source.getId());
        target.setTitle(source.getTitle());
        target.setDescription(source.getDescription());
        return target;
    }
}
