package madcake.backendmc.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AddressDTO {
    private Long id;
    @NotBlank(message = "Street name may not be a blank")
    private String street;
    @NotBlank(message = "House number name may not be a blank")
    private String houseNum;
    private String buildingNum;
    private String porch;
    private String floor;
    private String room;
}
