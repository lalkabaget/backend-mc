package madcake.backendmc.domain.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
@Data
public class ClientDTO {
    private Long id;
    @NotBlank(message = "Name may not be a blank")
    private String name;

    @Email(message = "Email should be valid")
    private String email;

    @NotBlank(message = "Phone number may not be a blank")
    @Pattern(regexp = "9[0-9]{9}")
    private String phone;

    @NotBlank(message = "username may not be a blank")
    private String username;

    @NotBlank(message = "password may not be a blank")
    private String password;

    //private Set<AddressDTO> addresses;
    //private Set<Long> ordersId;
    //private Set<String> roles;
}
