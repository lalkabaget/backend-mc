package madcake.backendmc.domain.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderDTO {
    private Long id;

    @NotNull
    private ClientDTO client;

    private LocalDateTime dateTime;

    @NotNull
    private AddressDTO address;

    private String paymentType;

    @NotNull
    @Size(min = 1, max = 50)
    private List<OrderItemDTO> orderItems;

    @NotNull
    @Min(value = 300, message = "Order total may not be less than 300")
    private BigDecimal total;
}
