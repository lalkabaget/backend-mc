package madcake.backendmc.domain.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class OrderItemDTO {
    private Long id;

    @Min(value = 1)
    private int quantity;

    @NotNull
    private ProductDTO product;

}
