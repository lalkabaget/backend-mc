package madcake.backendmc.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ProductDTO {
    private Long id;

    @NotBlank
    private String title;

    private String description;

    @NotNull
    private ProductGroupDTO productGroup;

    @NotNull
    private BigDecimal price;
}
