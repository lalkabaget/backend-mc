package madcake.backendmc.domain.dto;

import lombok.Data;

import java.util.Set;

@Data
public class ProductGroupDTO {
    private Long id;
    private String title;
    private String description;

}
