package madcake.backendmc.domain.dto;

import lombok.Data;

import java.util.Set;

@Data
public class RoleDTO {
    private Long id;
    private String title;
    private Set<Long> clientsId;
}
