package madcake.backendmc.domain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Address")
@Getter
@Setter
public class AddressEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "street")
    private String street;

    @NotBlank
    @Column(name = "house_num")
    private String houseNum;

    @Column(name = "building")
    private String buildingNum;

    @Column(name = "porch")
    private String porch;

    @Column(name = "floor")
    private String floor;

    @Column(name = "room")
    private String room;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private ClientEntity client;

    @NotNull
    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
    private Set<OrderEntity> orders = new HashSet<>();

}
