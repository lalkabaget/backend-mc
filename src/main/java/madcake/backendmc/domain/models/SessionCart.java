package madcake.backendmc.domain.models;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class SessionCart {
    private LocalDateTime initDate;
    private List<SessionCartItem> cartItems;
    private BigDecimal total;
    private int itemsCount;

    public SessionCart(LocalDateTime initDate) {
        cartItems = new ArrayList<>();
        total = new BigDecimal(0);
        this.initDate = initDate;
    }

}
