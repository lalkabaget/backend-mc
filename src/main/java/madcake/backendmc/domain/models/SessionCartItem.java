package madcake.backendmc.domain.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import madcake.backendmc.domain.dto.ProductDTO;

@Data
@AllArgsConstructor
public class SessionCartItem {
    private ProductDTO product;
    int quantity;
}
