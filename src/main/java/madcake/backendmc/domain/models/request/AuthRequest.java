package madcake.backendmc.domain.models.request;

import lombok.Data;

@Data
public class AuthRequest {
    private String username;
    private String password;
}
