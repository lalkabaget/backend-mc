package madcake.backendmc.domain.models.request;

import lombok.Data;
import madcake.backendmc.domain.dto.AddressDTO;

@Data
public class NewOrderRequest {
    private AddressDTO address;
}
