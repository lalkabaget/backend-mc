package madcake.backendmc.domain.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import madcake.backendmc.domain.models.SessionCart;

@Data
@AllArgsConstructor
public class CartItemResponse {
    private SessionCart sessionCart;

}
