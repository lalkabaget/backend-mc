package madcake.backendmc.domain.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.core.io.Resource;

@Data
@AllArgsConstructor
public class DownloadImageResponse {
    private Resource resource;
    private String originalName;

}
