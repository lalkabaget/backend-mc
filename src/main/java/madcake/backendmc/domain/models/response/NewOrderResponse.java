package madcake.backendmc.domain.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import madcake.backendmc.domain.dto.OrderDTO;

@Data
@AllArgsConstructor
public class NewOrderResponse {
    private OrderDTO orderDTO;
}
