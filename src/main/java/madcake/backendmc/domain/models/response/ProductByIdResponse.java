package madcake.backendmc.domain.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.models.SessionCart;

@Data
@AllArgsConstructor
public class ProductByIdResponse {
    private ProductDTO product;
    private SessionCart sessionCart;
}
