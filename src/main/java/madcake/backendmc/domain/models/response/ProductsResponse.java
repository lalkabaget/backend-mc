package madcake.backendmc.domain.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.models.SessionCart;

import java.util.List;

@Data
@AllArgsConstructor
public class ProductsResponse {
    private List<ProductDTO> products;
    private SessionCart sessionCart;
}
