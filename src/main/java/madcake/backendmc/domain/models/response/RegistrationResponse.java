package madcake.backendmc.domain.models.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import madcake.backendmc.domain.dto.ClientDTO;

@Data
@AllArgsConstructor
public class RegistrationResponse {
    private ClientDTO clientDTO;
}
