package madcake.backendmc.domain.repository;

import madcake.backendmc.domain.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity,Long> {
    ClientEntity findByUsername(String username);
}
