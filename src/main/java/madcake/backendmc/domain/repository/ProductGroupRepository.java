package madcake.backendmc.domain.repository;

import madcake.backendmc.domain.entity.ProductGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductGroupRepository extends JpaRepository<ProductGroupEntity, Long> {
}
