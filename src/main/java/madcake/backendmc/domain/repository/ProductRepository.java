package madcake.backendmc.domain.repository;

import madcake.backendmc.domain.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    @Query(value = "SELECT p.id, p.title, p.description, p.product_group_id, p.price " +
            "FROM Product p " +
            "JOIN Product_Group pg " +
            "ON p.product_group_id = pg.id " +
            "WHERE pg.title = :title",
            nativeQuery = true)
    List<ProductEntity> findProductsByGroup(@Param("title") String groupTitle);
}
