package madcake.backendmc.domain.repository;

import madcake.backendmc.domain.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity,Long> {
   RoleEntity findRoleByTitle(String title);
}
