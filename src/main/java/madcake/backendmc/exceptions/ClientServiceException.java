package madcake.backendmc.exceptions;

public class ClientServiceException extends Exception {
    public ClientServiceException(String message) {
        super(message);
    }
}
