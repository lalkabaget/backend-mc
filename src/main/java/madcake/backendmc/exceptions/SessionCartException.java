package madcake.backendmc.exceptions;

public class SessionCartException extends Exception {
    public SessionCartException(String message) {
        super(message);
    }
}
