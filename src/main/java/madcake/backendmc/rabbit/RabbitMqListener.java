package madcake.backendmc.rabbit;

import lombok.extern.slf4j.Slf4j;
import madcake.backendmc.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@EnableRabbit
@Slf4j
public class RabbitMqListener {

    private final MailService mailService;

    @Autowired
    public RabbitMqListener(MailService mailService) {
        this.mailService = mailService;
    }


    @RabbitListener(queues = "mailQueue")
    public void listenMailQueue(String username) {
        log.info("Received a message from mail queue. Sending message to user " + username);
        mailService.sendRegistrationNotification(username);
        log.info("Sent notification message to  " + username);

    }
}
