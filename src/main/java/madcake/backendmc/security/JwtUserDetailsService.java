package madcake.backendmc.security;

import madcake.backendmc.domain.entity.ClientEntity;
import madcake.backendmc.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    private final ClientService clientService;

    @Autowired
    public JwtUserDetailsService(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ClientEntity client = clientService.getClientByUsername(username);
        if(client == null) throw new UsernameNotFoundException("Account not found!");
        return new JwtUser(clientService.getClientByUsername(username));
    }
}
