package madcake.backendmc.service;

import madcake.backendmc.domain.entity.AddressEntity;
import madcake.backendmc.domain.entity.ClientEntity;
import madcake.backendmc.domain.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AddressService  {
    private final AddressRepository addressRepository;


    @Autowired
    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Transactional
    public AddressEntity addAddress(AddressEntity address, ClientEntity client) {
        address.setClient(client);
        return addressRepository.save(address);
    }
}
