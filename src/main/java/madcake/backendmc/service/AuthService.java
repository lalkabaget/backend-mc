package madcake.backendmc.service;

import madcake.backendmc.domain.entity.ClientEntity;
import madcake.backendmc.domain.entity.RoleEntity;
import madcake.backendmc.domain.models.request.AuthRequest;
import madcake.backendmc.domain.models.response.AuthResponse;
import madcake.backendmc.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final ClientService clientService;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, ClientService clientService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.clientService = clientService;
    }


    public AuthResponse getToken(AuthRequest authRequest) {
        try {
            String username = authRequest.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, authRequest.getPassword()));
            ClientEntity client = clientService.getClientByUsername(username);

            if (client == null) {
                throw new UsernameNotFoundException("Client with username: " + username + " not found");
            }

            String token =
                    jwtTokenProvider.createToken(username,
                            client.getRoles()
                                    .stream()
                                    .map(RoleEntity::getTitle)
                                    .collect(Collectors.toSet()));

            return new AuthResponse(username, token);

        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }

    }
}
