package madcake.backendmc.service;

import madcake.backendmc.domain.dto.ClientDTO;
import madcake.backendmc.domain.entity.ClientEntity;
import madcake.backendmc.domain.repository.ClientRepository;
import madcake.backendmc.exceptions.ClientServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientService {
    private final ClientRepository clientRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ClientService(ClientRepository clientRepository, RoleService roleService, PasswordEncoder passwordEncoder) {
        this.clientRepository = clientRepository;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public ClientEntity register(ClientDTO newClient) throws ClientServiceException {

        if (accountExist(newClient.getUsername())) {
            throw new ClientServiceException(
                    "There is an account with that username: "
                            + newClient.getUsername());
        }

        return clientRepository.save(createClientEntity(newClient));

    }

    @Transactional
    public ClientEntity getClientByUsername(String username) {
        ClientEntity client = clientRepository.findByUsername(username);
        if(client==null) throw new UsernameNotFoundException("Cannot find client by this username");
        return client;
    }


    private ClientEntity createClientEntity(ClientDTO newClient) {

        ClientEntity client = new ClientEntity();
        client.setName(newClient.getName());
        client.setPhone(newClient.getPhone());
        client.setEmail(newClient.getEmail());
        client.setUsername(newClient.getUsername());
        client.setPassword(passwordEncoder.encode(newClient.getPassword()));
        client.getRoles().add(roleService.getRole("ROLE_USER"));

        return client;
    }


    private boolean accountExist(String username) {
        ClientEntity client = clientRepository.findByUsername(username);
        return client != null;
    }



}
