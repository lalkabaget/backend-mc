package madcake.backendmc.service;

import madcake.backendmc.domain.entity.ClientEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailService {
   private final JavaMailSender javaMailSender;
   private final ClientService clientService;

    @Value("${spring.mail.username}")
    private String myMail;

    @Autowired
    public MailService(JavaMailSender javaMailSender, ClientService clientService) {
        this.javaMailSender = javaMailSender;
        this.clientService = clientService;
    }

    public void sendRegistrationNotification(String username) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        ClientEntity registeredClient = clientService.getClientByUsername(username);
        simpleMailMessage.setTo(registeredClient.getEmail());
        simpleMailMessage.setFrom(myMail);
        simpleMailMessage.setSubject("MadCake service notification");
        simpleMailMessage.setText("Thanks for registration, " + registeredClient.getName());
        javaMailSender.send(simpleMailMessage);

    }


}
