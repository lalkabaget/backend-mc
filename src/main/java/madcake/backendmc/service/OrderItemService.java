package madcake.backendmc.service;

import madcake.backendmc.domain.entity.OrderEntity;
import madcake.backendmc.domain.entity.OrderItemEntity;
import madcake.backendmc.domain.entity.ProductEntity;
import madcake.backendmc.domain.models.SessionCart;
import madcake.backendmc.domain.models.SessionCartItem;
import madcake.backendmc.domain.repository.OrderItemRepository;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderItemService {

    private final OrderItemRepository orderItemRepository;
    private final ConversionService conversionService;


    public OrderItemService(OrderItemRepository orderItemRepository,
                            ConversionService conversionService) {
        this.orderItemRepository = orderItemRepository;
        this.conversionService = conversionService;
    }

    @Transactional
    public OrderEntity addOrderItems(SessionCart cart, OrderEntity order) {
        List<OrderItemEntity> savedOrderItems = orderItemRepository.saveAll(getOrderItemsFromCart(cart, order));
        order.setOrderItems(savedOrderItems);
        return order;
    }

    /**
     * забираем товары из корзины и создаем на их
     * основе позиции заказа
     *
     * @param cart корзина с товаром из сессии
     * @param order заказ
     * @return список айтемов для order
     */
    private List<OrderItemEntity> getOrderItemsFromCart(SessionCart cart, OrderEntity order) {
        List<OrderItemEntity> orderItems = new ArrayList<>();

        for (SessionCartItem cartItem : cart.getCartItems()) {

            OrderItemEntity orderItem = new OrderItemEntity();
            orderItem.setQuantity(cartItem.getQuantity());
            orderItem.setProduct(conversionService.convert(cartItem.getProduct(), ProductEntity.class));
            orderItem.setOrder(order);

            orderItems.add(orderItem);
        }

        return orderItems;
    }

}
