package madcake.backendmc.service;

import madcake.backendmc.domain.entity.AddressEntity;
import madcake.backendmc.domain.entity.ClientEntity;
import madcake.backendmc.domain.entity.OrderEntity;
import madcake.backendmc.domain.models.SessionCart;
import madcake.backendmc.domain.models.SessionCartItem;
import madcake.backendmc.domain.repository.OrderRepository;
import madcake.backendmc.exceptions.OrderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final AddressService addressService;
    private final OrderItemService orderItemService;
    private final ClientService clientService;

    @Autowired
    public OrderService(OrderRepository orderRepository,
                        AddressService addressService,
                        OrderItemService orderItemService,
                        ClientService clientService) {
        this.orderRepository = orderRepository;
        this.addressService = addressService;
        this.orderItemService = orderItemService;
        this.clientService = clientService;
    }

    /**
     * создаем заказ в базе,
     * затем сохраняем все айтемы в сервисе ordersItem'ов
     * возвращаем заказ с айтемами
     *
     * @param cart     корзина из сессии
     * @param address  адрес от пользователя, который будет сохранен в базе
     * @param username логин пользователя
     * @return оформленный заказ
     */
    @Transactional
    public OrderEntity addOrder(SessionCart cart, AddressEntity address, String username) {
        OrderEntity newOrder = orderRepository.save(createOrderEntity(cart, address, username));
        return orderItemService.addOrderItems(cart, newOrder);
    }

    @Transactional
    public OrderEntity getOrderById(long id) {
        return orderRepository.findById(id).orElseThrow(() -> new OrderException("Order not found"));
    }

    @Transactional
    public List<OrderEntity> getAllOrders() {
        return orderRepository.findAll();
    }


    private OrderEntity createOrderEntity(SessionCart cart, AddressEntity address, String username) {
        OrderEntity order = new OrderEntity();

        ClientEntity client = clientService.getClientByUsername(username);

        AddressEntity newOrderAddress = addressService.addAddress(address, client);

        order.setClient(client);
        order.setAddress(newOrderAddress);
        order.setDateTime(LocalDateTime.now());
        order.setPaymentType("DEFAULT");
        order.setTotal(getOrderTotal(cart));

        return order;

    }

    private BigDecimal getOrderTotal(SessionCart cart) {
        BigDecimal total = new BigDecimal(0);
        if (cart.getCartItems() != null) {
            for (SessionCartItem cartItem : cart.getCartItems()) {
                BigDecimal itemPrice = cartItem.getProduct().getPrice();
                total = total.add(itemPrice.multiply(new BigDecimal(cartItem.getQuantity())));
            }
        }
        return total;
    }

}
