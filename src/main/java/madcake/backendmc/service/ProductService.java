package madcake.backendmc.service;

import madcake.backendmc.domain.entity.ProductEntity;
import madcake.backendmc.domain.repository.ProductRepository;
import madcake.backendmc.exceptions.ProductException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Transactional(readOnly = true)
    public List<ProductEntity> getAllProducts() {
        return productRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<ProductEntity> getProductsByGroup(String groupTitle) {
        return productRepository.findProductsByGroup(groupTitle);
    }

    @Transactional(readOnly = true)
    public ProductEntity getProductById(Long id) {
        return productRepository.findById(id).orElseThrow(() -> new ProductException("Product not found"));
    }


}
