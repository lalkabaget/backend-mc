package madcake.backendmc.service;

import madcake.backendmc.domain.entity.RoleEntity;
import madcake.backendmc.domain.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Transactional(readOnly = true)
    public RoleEntity getRole(String title) {
        return roleRepository.findRoleByTitle(title);
    }

}
