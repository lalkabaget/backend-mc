package madcake.backendmc.service;

import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.entity.ProductEntity;
import madcake.backendmc.domain.models.SessionCart;
import madcake.backendmc.domain.models.SessionCartItem;
import madcake.backendmc.exceptions.SessionCartException;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

@Service
public class SessionCartItemService {

    private final SessionCartService sessionCartService;
    private final ProductService productService;
    private final ConversionService conversionService;

    public SessionCartItemService(SessionCartService sessionCartService, ProductService productService, ConversionService conversionService) {
        this.sessionCartService = sessionCartService;
        this.productService = productService;
        this.conversionService = conversionService;
    }


    /**
     * Метод для добавления товара в корзину
     * Также, метож пересчитывает сумму стоимости товаров в корзине.
     *
     * @param session
     * @param productId
     * @return
     */
    public SessionCart addSessionCartItem(HttpSession session, Long productId) {
        SessionCart sessionCart = sessionCartService.getSessionCart(session);
        SessionCartItem newItem = getSessionCartItem(session, productId);

        if (newItem != null) {
            newItem.setQuantity(newItem.getQuantity() + 1);

            BigDecimal itemPrice = newItem.getProduct().getPrice();
            sessionCart.setTotal(sessionCart.getTotal().add(itemPrice));
        } else {
            ProductEntity product = productService.getProductById(productId);
            newItem = new SessionCartItem(conversionService.convert(product, ProductDTO.class), 1);
            sessionCart.getCartItems().add(newItem);

            BigDecimal newItemPrice = newItem.getProduct().getPrice();
            BigDecimal newTotal = sessionCart.getTotal().add(newItemPrice);
            sessionCart.setTotal(newTotal);
        }
        sessionCart.setItemsCount(sessionCart.getItemsCount() + 1);

        return sessionCart;
    }

    /**
     * Метод для удаления товара из корзины
     * Также, метод пересчитывает сумму стоимости товаров в корзине.
     *
     * @param session
     * @param productId
     * @return
     * @throws SessionCartException
     */
    public SessionCart removeSessionCartItem(HttpSession session, Long productId) throws SessionCartException {
        SessionCart sessionCart = sessionCartService.getSessionCart(session);

        SessionCartItem newItem = getSessionCartItem(session, productId);
        if (newItem != null) {

            if (newItem.getQuantity() > 1) {
                newItem.setQuantity(newItem.getQuantity() - 1);
            } else {
                sessionCart.getCartItems().remove(newItem);
            }
            BigDecimal itemPrice = newItem.getProduct().getPrice();
            BigDecimal newTotal = sessionCart.getTotal().subtract(itemPrice);
            sessionCart.setTotal(newTotal);

            sessionCart.setItemsCount(sessionCart.getItemsCount() - 1);
        } else throw new SessionCartException("This product not found in cart");

        return sessionCart;
    }

    private SessionCartItem getSessionCartItem(HttpSession session, Long productId) {
        SessionCart sessionCart = sessionCartService.getSessionCart(session);
        for (SessionCartItem ci : sessionCart.getCartItems()) {
            if (ci.getProduct().getId().equals(productId)) return ci;
        }
        return null;
    }


}
