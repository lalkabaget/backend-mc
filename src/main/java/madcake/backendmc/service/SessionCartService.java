package madcake.backendmc.service;

import madcake.backendmc.domain.models.SessionCart;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

@Service
public class SessionCartService {

    public SessionCart getSessionCart(HttpSession session) {
        if (session.getAttribute("cart") == null) {
            SessionCart sessionCart = new SessionCart(LocalDateTime.now());
            session.setAttribute("cart", sessionCart);
            return sessionCart;
        }
        return (SessionCart) session.getAttribute("cart");
    }
}
