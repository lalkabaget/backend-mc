create table client_role (
  client_id serial,
  role_id serial,

  FOREIGN KEY (client_id) REFERENCES Client(id),
  FOREIGN KEY (role_id) REFERENCES Role(id)
);