CREATE TABLE Product(
    id serial NOT NULL PRIMARY KEY,
	title varchar(50),
	description varchar(255),
	product_group_id serial NOT NULL,
	price numeric
);

CREATE TABLE Product_Group(
	id serial NOT NULL PRIMARY KEY,
	title varchar(50),
	description varchar(255)
);


CREATE TABLE Client(
	id serial NOT NULL PRIMARY KEY,
	name varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	phone varchar(50) NOT NULL,
	username varchar(50) NOT NULL,
	password varchar(255) NOT NULL
);



CREATE TABLE Address(
	id serial NOT NULL PRIMARY KEY,
	street varchar(255) NOT NULL,
	house_num varchar(10) NOT NULL,
	building varchar(25),
	porch varchar(15),
	floor varchar(10),
	room varchar(10),
	client_id serial NOT NULL
);



CREATE TABLE Ordr(
	id serial NOT NULL PRIMARY KEY,
	client_id serial NOT NULL,
	date TIMESTAMP,
	address_id serial NOT NULL,
	payment_type varchar(50),
	payment_status boolean,
	total numeric
);


CREATE TABLE Ordr_item(
	id serial NOT NULL PRIMARY KEY,
	quantity integer NOT NULL,
	product_id serial NOT NULL,
	order_id serial NOT NULL

);

ALTER TABLE Product ADD CONSTRAINT Product_fk0 FOREIGN KEY (product_group_id) REFERENCES Product_Group(id);

ALTER TABLE Address ADD CONSTRAINT Address_fk0 FOREIGN KEY (client_id) REFERENCES Client(id);

ALTER TABLE Ordr ADD CONSTRAINT Order_fk0 FOREIGN KEY (client_id) REFERENCES Client(id);
ALTER TABLE Ordr ADD CONSTRAINT Order_fk1 FOREIGN KEY (address_id) REFERENCES Address(id);

ALTER TABLE Ordr_item ADD CONSTRAINT Order_item_fk0 FOREIGN KEY (product_id) REFERENCES Product(id);
ALTER TABLE Ordr_item ADD CONSTRAINT Order_item_fk1 FOREIGN KEY (order_id) REFERENCES Ordr(id);

ALTER TABLE Ordr ALTER COLUMN payment_status SET DEFAULT 'false';
