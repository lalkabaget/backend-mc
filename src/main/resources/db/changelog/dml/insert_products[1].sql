insert into Product (title, description, product_group_id, price) values ('Классические панкейки без топпингов',  'some description', 1, 60);
insert into Product (title, description, product_group_id, price) values ('Классические панкейки с шоколадным топпингом',  'some description', 1, 100);
insert into Product (title, description, product_group_id, price) values ('Классические панкейки со сгущенкой',  'some description', 1, 90);
insert into Product (title, description, product_group_id, price) values ('Банановые панкейки с нутеллой',  'some description', 1, 140);
insert into Product (title, description, product_group_id, price) values ('Овсянные панкейки с клиновым сиропом',  'some description', 1, 160);
insert into Product (title, description, product_group_id, price) values ('Черничные панкейки с ягодным топпингом',  'some description', 1, 180);
insert into Product (title, description, product_group_id, price) values ('Капучино',  'some description', 2, 80);
insert into Product (title, description, product_group_id, price) values ('Черный чай',  'some description', 2, 50);
insert into Product (title, description, product_group_id, price) values ('Кола',  'some description', 3, 60);
insert into Product (title, description, product_group_id, price) values ('Сгущенка',  'some description', 4, 20);
insert into Product (title, description, product_group_id, price) values ('Шоколад',  'some description', 4, 25);
insert into Product (title, description, product_group_id, price) values ('Нутелла',  'some description', 4, 40);
insert into Product (title, description, product_group_id, price) values ('Клиновый сироп',  'some description', 4, 45);


