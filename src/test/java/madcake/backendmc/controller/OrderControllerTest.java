package madcake.backendmc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import madcake.backendmc.domain.dto.AddressDTO;
import madcake.backendmc.domain.dto.ProductDTO;
import madcake.backendmc.domain.dto.ProductGroupDTO;
import madcake.backendmc.domain.models.SessionCart;
import madcake.backendmc.domain.models.SessionCartItem;
import madcake.backendmc.models.TestOrderRequest;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("gevdev")
@Sql(value = {"/scripts/order_before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/scripts/order_after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Test
    public void doOrder() throws Exception {

        TestOrderRequest testRequest = createTestRequest();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);

        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(testRequest.getAddress());


        Map<String, Object> sessionAttrs = new HashMap<>();
        sessionAttrs.put("cart", testRequest.getSessionCart());



        this.mockMvc.perform(post("/api/order").sessionAttrs(sessionAttrs)
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orderDTO.id").value(IsNull.notNullValue()))
                .andExpect(jsonPath("$.orderDTO.client.id").value(IsNull.notNullValue()))
                .andExpect(jsonPath("$.orderDTO.client.name").value("George"))
                .andExpect(jsonPath("$.orderDTO.client.username").value("gevdev"))
                .andExpect(jsonPath("$.orderDTO.address.street").value("TestStreet"))
                .andExpect(jsonPath("$.orderDTO.address.houseNum").value("1"))
                .andExpect(jsonPath("$.orderDTO.address.buildingNum").value("1"))
                .andExpect(jsonPath("$.orderDTO.address.porch").value("1"))
                .andExpect(jsonPath("$.orderDTO.address.floor").value("1"))
                .andExpect(jsonPath("$.orderDTO.address.room").value("1"))
                .andExpect(jsonPath("$.orderDTO.total").value(5000))
                .andExpect(jsonPath("$.orderDTO.paymentType").value("DEFAULT"));

    }


    private TestOrderRequest createTestRequest() {


        AddressDTO newOrderAddress = new AddressDTO();
        newOrderAddress.setStreet("TestStreet");
        newOrderAddress.setHouseNum("1");
        newOrderAddress.setBuildingNum("1");
        newOrderAddress.setPorch("1");
        newOrderAddress.setFloor("1");
        newOrderAddress.setRoom("1");

        SessionCart sessionCart = new SessionCart(LocalDateTime.now());

        ProductGroupDTO productGroup = new ProductGroupDTO();
        productGroup.setId(1L);
        productGroup.setTitle("Панкейки");

        ProductDTO product = new ProductDTO();
        product.setId(1L);
        product.setTitle("test-pancake");
        product.setPrice(new BigDecimal(5000));
        product.setProductGroup(productGroup);

        SessionCartItem sessionCartItem = new SessionCartItem(product,1);

        sessionCart.getCartItems().add(sessionCartItem);


        return new TestOrderRequest(sessionCart,newOrderAddress);
    }
}