package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.AddressDTO;
import madcake.backendmc.domain.entity.AddressEntity;
import madcake.backendmc.domain.entity.ClientEntity;
import madcake.backendmc.service.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AddressDtoToAddressEntityConverterTest {

    @InjectMocks
    private AddressDtoToAddressEntityConverter addressDtoToAddressEntityConverter;

    private AddressDTO addressDTO = new AddressDTO();
    @Before
    public void setUp() {

        addressDTO.setId(1L);
        addressDTO.setStreet("PUSHKINA");
        addressDTO.setBuildingNum("1");
        addressDTO.setHouseNum("5");
        addressDTO.setPorch("2");
        addressDTO.setFloor("16");
        addressDTO.setRoom("155");

    }


    @Test
    public void correctConverting() {


        assertThat(addressDtoToAddressEntityConverter.convert(addressDTO), instanceOf(AddressEntity.class));
        AddressEntity address = addressDtoToAddressEntityConverter.convert(addressDTO);

        assertEquals(address.getId(), addressDTO.getId());
        assertEquals(address.getStreet(), addressDTO.getStreet());
        assertEquals(address.getBuildingNum(), addressDTO.getBuildingNum());
        assertEquals(address.getHouseNum(), addressDTO.getHouseNum());
        assertEquals(address.getPorch(), addressDTO.getPorch());
        assertEquals(address.getFloor(), addressDTO.getFloor());
        assertEquals(address.getRoom(), addressDTO.getRoom());

    }
}