package madcake.backendmc.converter;

import madcake.backendmc.domain.dto.AddressDTO;
import madcake.backendmc.domain.entity.AddressEntity;
import madcake.backendmc.domain.entity.ClientEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AddressEntityToAddressDtoConverterTest {

    @InjectMocks
    AddressEntityToAddressDtoConverter addressEntityToAddressDtoConverter;

    private AddressEntity address = new AddressEntity();
    @Before
    public void setUp() {

        address.setId(123L);
        address.setStreet("PUSHKINA");
        address.setBuildingNum("1");
        address.setHouseNum("5");
        address.setPorch("2");
        address.setFloor("16");
        address.setRoom("155");
    }

    @Test
    public void correctConverting() {

        assertThat(addressEntityToAddressDtoConverter.convert(address), instanceOf(AddressDTO.class));

        AddressDTO addressDTO = addressEntityToAddressDtoConverter.convert(address);

        assertEquals(addressDTO.getId(), address.getId());
        assertEquals(addressDTO.getStreet(), address.getStreet());
        assertEquals(addressDTO.getBuildingNum(), address.getBuildingNum());
        assertEquals(addressDTO.getHouseNum(), address.getHouseNum());
        assertEquals(addressDTO.getPorch(), address.getPorch());
        assertEquals(addressDTO.getFloor(), address.getFloor());
        assertEquals(addressDTO.getRoom(), address.getRoom());


    }

}