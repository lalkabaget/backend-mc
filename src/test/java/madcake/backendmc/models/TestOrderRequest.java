package madcake.backendmc.models;

import madcake.backendmc.domain.dto.AddressDTO;
import madcake.backendmc.domain.models.SessionCart;


public class TestOrderRequest {
    private SessionCart sessionCart;
    private AddressDTO address;

    public TestOrderRequest(SessionCart sessionCart, AddressDTO address) {
        this.sessionCart = sessionCart;
        this.address = address;
    }

    public SessionCart getSessionCart() {
        return sessionCart;
    }

    public void setSessionCart(SessionCart sessionCart) {
        this.sessionCart = sessionCart;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
