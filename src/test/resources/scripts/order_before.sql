delete from ordr_item;
delete from ordr;
delete from product;
delete from product_group;
delete from address;
delete from client_role;
delete from client;

insert into client (id, name, email, phone, username, password) values (1, 'George', 'gevdev15rus@gmail.com', '1234557777', 'gevdev', '$2a$10$QS6Q0eo57uAEOuby7rnQ2.wdCMvWmJpvOSV0iEmyMuKPQpQDDgUHq');
insert into client_role (client_id, role_id) values (1, 2);
insert into Product_Group (id, title) values (1, 'Панкейки');
insert into Product (id, title, description, product_group_id, price) values (1, 'test-pancake', 'some description', 1, 40);




